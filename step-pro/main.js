//     Вкладки в секции Our services должны переключаться при нажатии мышью. Текст и картинки для других вкладок вставить любые.
//     Кнопка Load more в секции Our amazing work имитирует подгрузку с сервера новых картинок.
//     При ее нажатии в секции снизу должно добавиться еще 12 картинок (изображения можно взять любые). После этого кнопка исчезает.
//     Кнопки на вкладке Our amazing work являются "фильтрами продукции". Предварительно каждой из картинок нужно присвоить одну из четырех категорий,
//     на ваше усмотрение (на макете это Graphic design, Web design, Landing pages, Wordpress).
//     При нажатии на кнопку категории необходимо показать только те картинки, которые относятся к данной категории.
//     All показывает картинки из всех категорий. Категории можно переименовать, картинки для категорий взять любые.
//     Карусель на вкладке What people say about theHam должна быть рабочей, по клику как на иконку фотографии внизу, так и на стрелки вправо-влево.
//     В карусели должна меняться как картинка, так и текст. Карусель обязательно должна быть с анимацией.
//     Для подключения динамических элементов можно использовать любые библиотеки - как jQuery/его плагины, так и чистый Javascript код.

/// ВСМПОМОГАТЕЛЬНЫЕ ФУНКЦИИ

// ВЕРНУТЬ СЛУЧАЙНЫЙ ИНДЕКС
function getRndIndex(start, end) {
    return Math.floor(Math.random() * (end - start + 1) ) + start;
}

// СКРЫТЬ ЭЛЕМЕНТ
function hideElement(el) {
    el.classList.add("hidden");
}

// ПОКАЗАТЬ ЭЛЕМЕНТ
function unHideElement(el) {
    el.classList.remove("hidden");
}

/// БЛОК OUR SERVICES

let ourServicesTabs = [...document.querySelectorAll(".service-container-links-item")];
ourServicesTabs.forEach(element => element.addEventListener("click", servicesTabToggle));

let ourServiceContents = [...document.querySelectorAll(".service-container-info-item")];

// ПЕРЕКЛЮЧИТЬ ВКЛАДКУ
function servicesTabToggle(event) {
    event.target.closest("ul").querySelector(".tabActive").classList.remove("tabActive");
    event.target.classList.add("tabActive");

    const index = ourServicesTabs.findIndex(element => element.classList.contains("tabActive"));

    tabShowTiedText(index, ourServiceContents)
}

// ПЕРЕКЛЮЧИТЬ ТЕКСТ, ПРИВЯЗАННЫЙ КО ВКЛАДКЕ
function tabShowTiedText(index, tiedTextArr){
    tiedTextArr.forEach(hideElement);
    unHideElement(tiedTextArr[index]);
}


/// БЛОК OUR AMAZING WORK

let ourFilterTabs = [...document.querySelectorAll(".amazing-container-item")];
ourFilterTabs.forEach(element => element.addEventListener("click", amazingTabToggle));

let ourFilterContents = [...document.querySelectorAll(".amazing-container-gallery li")];

let ourFilters = ["ALL", "GD", "WD", "LP", "WP"];

// ПЕРЕКЛЮЧИТЬ ВКЛАДКУ
function amazingTabToggle(event) {
    event.target.closest("ul").querySelector(".tabActive").classList.remove("tabActive");
    event.target.classList.add("tabActive");

    const index = ourFilterTabs.findIndex(element => element.classList.contains("tabActive"));

    applyAmazingFilter(index, ourFilterContents)
}

// ПОКАЗАТЬ ЗАГРУЖЕННЫЙ КОНТЕНТ С ФИЛЬТРОМ ПО ВКЛАДКЕ
function applyAmazingFilter(index, tiedTextArr){
    tiedTextArr.forEach(function(element) {
        hideElement(element);
        if (element.classList.contains(`amazing-container-gallery-item_${ourFilters[index]}`) && element.classList.contains("loaded")) {
            unHideElement(element);
        }
    });
}

// ПОДГРУЗИТЬ ЕЩЁ 12 ЭЛЕМЕНТОВ
function amazingContentStepShow() {
    let toLoad = ourFilterContents.slice(ourFilterContents.findIndex(element => !element.classList.contains("loaded")), ourFilterContents.length)
    for (let i = 0; i < 12; i++) {
        toLoad[i].classList.add("loaded");
    }
}

// ПЕРЕКЛЮЧИТЬ КОНТЕНТ (ПО ХОВЕРУ)
ourFilterContents.forEach(element => element.addEventListener("mouseenter", toggleAmazingPreview));
ourFilterContents.forEach(element => element.addEventListener("mouseleave", toggleAmazingPreview));

function toggleAmazingPreview(event) {
    if (event.type === "mouseenter") {
        unHideElement(event.target.querySelector(".amazing-gallery-preview"));
        hideElement(event.target.querySelector("img"));
    } else {
        hideElement(event.target.querySelector(".amazing-gallery-preview"));
        unHideElement(event.target.querySelector("img"));
    }
}

// КНОПКА LOAD MORE

let ourAmazingGalleryPlus = document.querySelector(".amazing-container .loadMore");
let loadMoreButton = document.querySelector(".amazing-container .amazing-button");
let clickCount = 0

// КЛИК ПО КНОПКЕ (ТАЙМ-АУТ 2 СЕК)
loadMoreButton.addEventListener("click", function (event) {
    clickCount++;
    const prevBtnHTML = event.target.innerHTML;
    event.target.innerHTML = 'Loading...';
    setTimeout(appendGallery, 2000);
    setTimeout(function () {
        event.target.innerHTML = prevBtnHTML;
    }, 2000);
    if (clickCount > 1) {
        setTimeout(function () {
            hideElement(event.target)
        }, 2000);
    }
});

// ПОКАЗАТЬ ДОП КАРТИНКИ
function appendGallery() {
    ourAmazingGalleryPlus.querySelectorAll("li").forEach(element => {
        setFilterGroups(element);
    });
    amazingContentStepShow();
    applyAmazingFilter(ourFilterTabs.findIndex(element => element.classList.contains("tabActive")), ourFilterContents);

    unHideElement(ourAmazingGalleryPlus);
}

// ПРОСТАВИТЬ ФИЛЬТРЫ ДОП КАРТИНКАМ
function setFilterGroups(el) {
    let addFilters = true;
    ourFilters.forEach(filter => {
        if (el.classList.contains(`amazing-container-gallery-item_${filter}`)) {
            if (filter !== "ALL") {
                addFilters = false;
            }
        }
    });
    if (addFilters) {
        el.classList.add(`amazing-container-gallery-item_${ourFilters[getRndIndex(1, ourFilters.length-1)]}`);
    }
}

/// БЛОК BREAKING NEWS

let ourNewsItems = document.querySelectorAll(".breakingNews-gallery li");

ourNewsItems.forEach(element => element.addEventListener("mouseenter", toggleNewsHighlight));
ourNewsItems.forEach(element => element.addEventListener("mouseleave", toggleNewsHighlight))

// ПОДСВЕТИТЬ СВЯЗАННЫЙ КОНТЕНТ

function toggleNewsHighlight(event) {
    if (event.type === "mouseenter") {
        event.target.querySelector(".breaking-heading").classList.add("breaking-heading_highlight");
        event.target.querySelector(".date").classList.add("date_highlight");

    } else {
        event.target.querySelector(".breaking-heading").classList.remove("breaking-heading_highlight");
        event.target.querySelector(".date").classList.remove("date_highlight");
    }
}

// DYNAMIC DATE

document.querySelectorAll(".breakingNews-gallery-item .date .today").forEach(function(el) {
    const d = new Date();
    const month = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "June",
        "July",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    ]
    el.innerHTML = `${d.getDay()} <br> ${month[d.getMonth()]} `
})

/// БЛОК С КАРУСЕЛЬЮ

let ourClientPhotos = [...document.querySelectorAll(".person-gallery .person-photo .photo")];
let ourClientAbout = [...document.querySelectorAll(".person-about .person-item")]

// ВРАЩАТЬ ПО КЛИКУ НА ИКОНКУ
ourClientPhotos.forEach(element => element.addEventListener("click", function() {
        spinCarousel(event.target);
    })
);

// ВРАЩАТЬ К ЭЛЕМЕНТУ
function spinCarousel(el) {
    ourClientPhotos.forEach(element => element.classList.remove("photo_clicked"));
    el.classList.add("photo_clicked");

    toggleAbout();
}

// ПЕРЕКЛЮЧИТЬ ИНФО (ИМЯ, ПРОФЕССИЯ)
function toggleAbout() {
    ourClientAbout.forEach(hideElement);

    const index = ourClientPhotos.findIndex(element => element.classList.contains("photo_clicked"));
    unHideElement(ourClientAbout[index]);
}


let nextButtonCarousel = document.querySelector(".person-carousel .rignt-arrow");
let prevButtonCarousel = document.querySelector(".person-carousel .left-arrow");

// ВРАЩАТЬ К СЛЕДУЮЩЕМУ
nextButtonCarousel.addEventListener("click", function () {
    document.querySelector(".photo_clicked").animate({
        width: "90px",
        height: "90px"
    }, 200, function() {
    });
    spinCarousel(getNextClient());
});

// ВРАЩАТЬ К ПРЕДЫДУЩЕМУ
prevButtonCarousel.addEventListener("click", function () {
    document.querySelector(".photo_clicked").animate({
        width: "90px",
        height: "90px"
    }, 200, function() {
    });
    spinCarousel(getPrevClient());
});

// ВЕРНУТЬ СЛЕД ЭЕЛЕМЕНТ КАРУСЕЛИ
function getNextClient() {
    let currentClient = document.querySelector(".photo_clicked");
    let nextClient = ourClientPhotos[0];
    ourClientPhotos.forEach(el => {
        if (ourClientPhotos.indexOf(el) === ourClientPhotos.indexOf(currentClient)) {
            if (ourClientPhotos.indexOf(el) !== ourClientPhotos.length-1) {
                nextClient = ourClientPhotos[ourClientPhotos.indexOf(el)+1];
            }
        }
    });
    return nextClient;
}

// ВЕРНУТЬ ПРЕД ЭЛЕМЕНТ КАРУСЕЛИ
function getPrevClient() {
    let currentClient = document.querySelector(".photo_clicked");
    let prevClient = ourClientPhotos[ourClientPhotos.length-1];
    ourClientPhotos.forEach(el => {
        if (ourClientPhotos.indexOf(el) === ourClientPhotos.indexOf(currentClient)) {
            if (ourClientPhotos.indexOf(el) !== 0) {
                prevClient = ourClientPhotos[ourClientPhotos.indexOf(el)-1];
            }
        }
    });
    return prevClient;
}

// FOOTER COPYRIGHT DYNAMIC

document.querySelector(".footer-info .copyrightDate").innerText = new Date().getFullYear();

// ПОДГРУЗИТЬ ПЕРВЫЕ 12 ЭЛЕМЕНТОВ
amazingContentStepShow()
