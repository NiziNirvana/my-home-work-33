/*
Взять любое готовое домашнее задание по HTML/CSS.
    Добавить на макете кнопку "Сменить тему".
    При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
    Выбранная тема должна сохраняться и после перезагрузки страницы*/

// Set Theme
let cTheme = localStorage.getItem('currentTheme');

let btn = document.getElementById("theme-btn");
let link = document.getElementById("theme-link");

link.setAttribute("href", cTheme);

btn.addEventListener("click", function () {
    ChangeTheme();
});

function ChangeTheme()
{
    let lightTheme = "./css/light.css";
    let darkTheme = "./css/dark.css";
    let currTheme = link.getAttribute("href");

    if(currTheme == lightTheme)
    {
        currTheme = darkTheme;
    }
    else
    {
        currTheme = lightTheme;
    }

    link.setAttribute("href", currTheme);
    localStorage.setItem('currentTheme', link.getAttribute("href"));
}
