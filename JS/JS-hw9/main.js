/**

В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для
 нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться
 для какой вкладки.
    Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
    Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
 При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.*/

/*
let textListIds = ['text-0', 'text-1', 'text-2', 'text-3', 'text-4'];
let tabIds = ['0', '1', '2', '3', '4'];


function handler(id) {
    textListIds.forEach(function (item) {
        if (`text-${id}` === item) {
            document.getElementById(item).style.display = 'block';
            document.querySelectorAll(".tabs-title").forEach(tab => {
                tab.classList.remove('active')
            })
            document.getElementById(id).classList.add('active');
        } else {
            document.getElementById(item).style.display = 'none';
           /!* document.getElementById(id).classList.remove('active'); *!/
        }
    });
toggle переключения
target цель события
closest ближайший елемент с таким тегом
contains = содержит
[...document.querySelectorAll(".tabs-title")]; копирование массива спред оператор
}
*/

let tabs = [...document.querySelectorAll(".tabs-title")];
let tabsContent = document.querySelectorAll(".tabs-content li");



tabs.forEach(element => element.addEventListener("click",toggle))

function toggle(event){
    event.target.closest("ul").querySelector(".active").classList.remove("active");
    event.target.classList.add("active");
    const index = tabs.findIndex(element => element.classList.contains("active"));

    showText(index)
}
function showText(index){
    console.log(index)
    tabsContent.forEach(element => element.setAttribute("hidden", true));
    tabsContent[index].removeAttribute("hidden")
}