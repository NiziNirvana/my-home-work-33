/**Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
экранирование нужно для того что бы могли вводить спец символы их можно ввести с помощью \
для того что бы вывести например ( нам надо перед ней поставить обратный слеш
 */

function createNewUser() {
    let newUser = {
        firstName: prompt('Enter First Name'),
        lastName: prompt('Enter Last Name'),
        birthday: prompt('Enter birth date'),
        getLogin: function(){
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function(){
            let currentDate = new Date();
            let birthDate = new Date(this.birthday);
            if(currentDate.getMonth() > birthDate.getMonth()) {
                return  currentDate.getFullYear() - birthDate.getFullYear();
            }
            else {
                if(currentDate.getDay() > birthDate.getDay()) {
                    return currentDate.getFullYear() - birthDate.getFullYear();
                } else {
                    return  currentDate.getFullYear() - birthDate.getFullYear() - 1;
                }
            }
        },
        getPassword: function(){
            let birthDate = new Date(this.birthday);
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + birthDate.getFullYear();
        },
    };
    return newUser;
}

user01 = createNewUser();

console.log(user01);
console.log("User Login: " + user01.getLogin());
console.log("User Age: " + user01.getAge());
console.log("User Pass: " + user01.getPassword());
