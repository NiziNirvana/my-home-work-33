/**
При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
Поведение поля должно быть следующим:

    При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
    Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст:
 Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода
 окрашивается в зеленый цвет.
    При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
    Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить
 фразу - Please enter correct price. span со значением при этом не создается.


    В папке img лежат примеры реализации поля ввода и создающегося span.*/


myInput = document.getElementsByClassName('input-block')[0];
elPlace = elPlace = document.getElementsByClassName('placeholder')[0];

function createElem(tag, myClass, attrs, text, onclick=null) {


    let newElem = document.createElement(tag);

    newElem.setAttribute('class', myClass);
    newElem.setAttribute('style', attrs);
    newElem.setAttribute('onclick', onclick);
    newElem.innerText = text;

    return newElem;
}

function deleteElem(myClass) {
    let el = document.getElementsByClassName(myClass)[0];
    el.remove();
}

function onFocus() {

        elPlace.classList.add("pl-any");
        elPlace.classList.remove("pl-neg");
        elPlace.classList.remove("pl-pos");

}

function onFocusOut() {

    if(elPlace.value >= 0){
        elPlace.classList.remove("pl-any");
        elPlace.classList.remove("pl-neg");
        elPlace.classList.add("pl-pos");
    } else {
        elPlace.classList.remove("pl-any");
        elPlace.classList.remove("pl-pos");
        elPlace.classList.add("pl-neg");
    }

    if(elPlace.value >= 0){
        let myUpStr = createElem('span', 'myTextUp', 'color:blue', `Текущая цена: ${elPlace.value} `);
        document.body.insertBefore(myUpStr, myInput);

        let myBtn = createElem('button', 'myBtn', null, 'X', "onClick()");
        myUpStr.appendChild(myBtn);

        let myBr = createElem('br', 'myBr', null, '\\/');
        myBtn.appendChild(myBr);


    } else {
        let myDownStr = createElem('div', 'myTextDown', 'color:blue', 'Please enter correct price');
        myInput.appendChild(myDownStr);
    }
}

function onClick() {
    deleteElem('myBtn');
    deleteElem('myTextUp');
    elPlace.value = null;
}