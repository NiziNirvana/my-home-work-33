/**
При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
Поведение поля должно быть следующим:

    При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
    Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст:
 Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода
 окрашивается в зеленый цвет.
    При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
    Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить
 фразу - Please enter correct price. span со значением при этом не создается.


    В папке img лежат примеры реализации поля ввода и создающегося span.*/
let elPlace = document.getElementsByClassName('placeholder')[0];
let elDiv = document.getElementsByClassName('input')[0];

let elSpanUp = document.createElement('span');
let elSpanDown = document.createElement('span');
let elBrUp = document.createElement('br');
let elBrDown = document.createElement('br');
let elBtn = document.createElement('button');

elSpanUp.innerHTML = `Текущая цена: ${elPlace.value}`;
elSpanDown.innerHTML = 'Please enter correct price.';
elBrUp.innerHTML = '';
elBrDown.innerHTML = '';
elBtn.innerHTML = 'X';
elDiv.insertBefore(elSpanUp, elPlace);
elDiv.insertBefore(elBtn, elPlace);
elDiv.insertBefore(elBrUp, elPlace);
elDiv.appendChild(elBrDown);
elDiv.appendChild(elSpanDown);
elSpanUp.style.visibility = 'hidden';
elSpanDown.style.visibility = 'hidden';
elBrUp.style.visibility = 'hidden';
elBrDown.style.visibility = 'hidden';
elBtn.style.visibility = 'hidden';

function onFocus() {

    if(elPlace.value >= 0){
        // showInputValid()
        elPlace.style.color = 'green';
        elPlace.style.border = '2px solid green';
    } else {
        // showInputError()
        elPlace.style.color = 'red';
        elPlace.style.border = '2px solid red';
    }

}

function onFocusOut() {

    if(elPlace.value >= 0){
        elPlace.style.color = 'green';
        elPlace.style.border = '2px solid green';
        elSpanUp.style.visibility = 'visible';
        elBrUp.style.visibility = 'visible';
        elBtn.style.visibility = 'visible';

        elBrDown.style.visibility = 'hidden';
        elSpanDown.style.visibility = 'hidden';


    } else {
        elPlace.style.color = 'red';
        elPlace.style.border = '2px solid red';
        elBrDown.style.visibility = 'visible';
        elSpanDown.style.visibility = 'visible';

        elSpanUp.style.visibility = 'hidden';
        elBrUp.style.visibility = 'hidden';
        elBtn.style.visibility = 'hidden';
    }
}
//isArray
elBtn.onclick = function() {
    elSpanUp.style.visibility = 'hidden';
    elBrUp.style.visibility = 'hidden';
    elBtn.style.visibility = 'hidden';
    elPlace.value = 0;
}
