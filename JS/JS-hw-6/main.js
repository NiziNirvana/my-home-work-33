let initArray = ['hello', 'world', 23, '23', null];
let dataType = prompt('Enter Data Type', 'string');
function filterBy(array, dataType) {
    let filteredArray = [];
    array.forEach(function(item) {
        if(typeof item !== dataType) {
            filteredArray.push(item);
        }
    })
    return filteredArray
}

console.log(filterBy(initArray,dataType));
