/*
Технические требования:

    В папке banners лежит HTML код и папка с картинками.
    При запуске программы на экране должна отображаться первая картинка.
    Через 3 секунды вместо нее должна быть показана вторая картинка.
    Еще через 3 секунды - третья.
    Еще через 3 секунды - четвертая.
    После того, как покажутся все картинки - этот цикл должен начаться заново.
    При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
    По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка,
     которая была там при нажатии кнопки.
    Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл
    продолжается с той картинки, которая в данный момент показана на экране.
    Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.


    Необязательное задание продвинутой сложности:

    При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий
     сколько осталось до показа следующей картинки.
    Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.*/

/*let slides = document.querySelectorAll('#slides .image-to-show')
let currentSlide = 0
let slideInterval = setInterval(nextImage, 3000)

function nextImage() {
    slides[currentSlide].setAttribute('hidden', 'true')
    currentSlide = (currentSlide + 1) % slides.length
    slides[currentSlide].removeAttribute('hidden');
    // console.log(currentSlide)
}

document.getElementById('pause').addEventListener('click', pauseSlideshow)
document.getElementById('play').addEventListener('click', playSlideshow)

function pauseSlideshow() {
    clearInterval(slideInterval)
}

function playSlideshow() {
    slideInterval = setInterval(nextImage, 3000)
}*/

let slides = document.querySelectorAll('#images-wrapper .image-to-show')
console.log(slides);
let currentSlide = 0
let slideInterval = setInterval(nextImage, 2000)
let buttonsInterval = setTimeout(hideBtn, 3000)
let playing = true
let pauseButton = document.getElementById('pause')
let playButton = document.getElementById('play')
pauseButton.addEventListener('click', pauseImageShow)
playButton.addEventListener('click', playImageShow)

function nextImage() {
    slides[currentSlide].classList.add("hide");
    currentSlide = (currentSlide + 1) % slides.length
    slides[currentSlide].classList.remove("hide");
}

function hideBtn() {
    pauseButton.classList.remove('hide')
}

function pauseImageShow() {
    playing = false
    clearInterval(slideInterval)
    pauseButton.classList.add('hide')
    playButton.classList.remove('hide')
}

function playImageShow() {
    playing = true
    slideInterval = setInterval(nextImage, 2000)
    pauseButton.classList.remove('hide')
    playButton.classList.add('hide')
}