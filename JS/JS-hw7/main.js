/**
Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент
 parent - DOM-элемент, к которому будет прикреплен список
 (по дефолту должен быть document.body).
Каждый из элементов массива вывести на страницу в виде пункта списка;
Используйте шаблонные строки и метод map массива для формирования контента списка
 перед выведением его на страницу;

Примеры массивов, которые можно выводить на экран:
    ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
["1", "2", "3", "sea", "user", 23];

Можно взять любой другой массив.


    Необязательные задания продвинутой сложности:


    Добавьте обработку вложенных массивов. Если внутри массива одним из элементов
 будет еще один массив, выводить его как вложенный список.
    Пример такого массива:
    ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

Подсказка: используйте map для обхода массива и рекурсию, чтоб обработать
 вложенные массивы.*/


let initList = ["Kharkiv", "Kiev",  ["Borispol", ['Airport'], "Irpin"], "Odessa", "Lviv", "Dnieper"];

function toHtmlList(el){
    if (typeof el === 'object') {
        return `<ul>${el.map(toHtmlList).join('')}</ul>`;
    } else {
        return `<li>${el}</li>`;
    }
}

function createList(list, elementName= document.body){
    let elementList = "<ol>";
    alert(list.map(toHtmlList).join(''));
    elementList += list.map(toHtmlList).join('');
    elementList += "</ol>";
    elementName.innerHTML += elementList;
}

createList(initList, document.getElementsByTagName('div')[0]);


