/**Теоретический вопрос

Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
Почему объявлять переменную через var считается плохим тоном?


    Задание
    Реализовать простую программу на Javascript, которая будет взаимодействовать с пользователем
    с помощью модальных окон браузера - alert, prompt, confirm. Задача должна быть реализована
    на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

    Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
    Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
    Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением:
    Are you sure you want to continue? и кнопками Ok, Cancel. Если пользователь нажал Ok,
    показать на экране сообщение: Welcome,  + имя пользователя. Если пользователь нажал Cancel,
    показать на экране сообщение: You are not allowed to visit this website.
    Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.
    Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.


    Необязательное задание продвинутой сложности:

    После ввода данных добавить проверку их корректности.
    Если пользователь не ввел имя, либо при вводе возраста
    указал не число - спросить имя и возраст заново
    (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).*/


/**1.Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
 *Переменные, объявленные при помощи var, могут иметь как глобальную, так и локальную область видимости.
 объявленная при помощи оператора var вне блока функции, будет доступна во всем window будет глобальной
 Но var внутри функции то  она имеет локальную область видимости. Это означает, что доступ к ней есть
 только в рамках этой функции.Переменные, объявленные при помощи var, могут как объявляться заново, так и обновляться
 переменные, объявленные при помощи var, поднимаются в верх своей области видимости и инициализируются присвоением
 какого-нибудь значения или undefined.

 let=имеет блочную область видимости.Переменная, объявленная в блоке кода при помощи оператора let, доступна для
 использования только в рамках этого блока кода.Переменные, объявленные при помощи let, могут обновляться,
 но не объявляться повторно.

 Const=Переменные, объявленные с использованием const, содержат постоянные значения.
 Переменные, объявленные при помощи const, нельзя обновить или объявить заново

 2.Почему объявлять переменную через var считается плохим тоном?

 Если объявить переменную с использованием var, то обратиться к ней в функции можно и после выхода из той конструкции,
 где она была объявлена. Это может превратиться в проблему тогда, когда функции усложняются.

 */

    let userName = prompt('Your name ?', 'Name');
    let userAge = +prompt('How old are you ?','18');
     if(userAge < 18) {
           alert('You are not allowed to visit this website.');
        }
     else if(userAge <= 22) {

           const isContinue = confirm('Are you sure you want to continue?');
           if (isContinue) {
               alert('Welcome ' +userName)
           }
           else {
               alert('You are not allowed to visit this website');
           }

        }
        else {
            alert(' Welcome ' +userName);
        }

