/*
Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript,
без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

В файле index.html лежит разметка для двух полей ввода пароля.
По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь,
иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения

После нажатия на кнопку страница не должна перезагружаться
Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.*/

let pw1 = document.getElementById("input-password");
let iconPw1Visible = document.getElementById("password");
let iconPw1Hidden = document.getElementById("password-hidden");

let pw2 = document.getElementById("input-confirm-password");
let iconPw2Visible = document.getElementById("confirm-password");
let iconPw2Hidden = document.getElementById("confirm");

let btn = document.getElementById("btn");

let warning =  document.getElementById("warning");

iconPw1Visible.addEventListener("click", function () {
    togglePassword(pw1, iconPw1Visible, iconPw1Hidden);
});
iconPw1Hidden.addEventListener("click", function () {
    togglePassword(pw1, iconPw1Visible, iconPw1Hidden);
});
iconPw2Visible.addEventListener("click", function () {
    togglePassword(pw2, iconPw2Visible, iconPw2Hidden);
});
iconPw2Hidden.addEventListener("click", function () {
    togglePassword(pw2, iconPw2Visible, iconPw2Hidden);
});

btn.addEventListener("click",comparePassword);

function togglePassword (elem, icon, icon_hidden) {
     if(elem.getAttribute("type") === "password") {
        elem.setAttribute("type","text");
        icon.classList.add("hidden");
        icon_hidden.classList.remove("hidden");
     } else {
        elem.setAttribute("type","password");
        icon.classList.remove("hidden");
        icon_hidden.classList.add("hidden");
     }

}

function comparePassword(event) {
    if (pw1.value === pw2.value) {
        warning.classList.add("hidden");
        alert("You are welcome");
    }else {
        warning.classList.remove("hidden");
    }
    event.preventDefault();
}
//подскажите пожалуйста подскажите почему  alert отрабатывает раньше хотя позже

