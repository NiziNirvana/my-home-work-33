/*
Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
    При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
    Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться
     кнопка "Наверх" с фиксированным позиционариванием. При клике на нее - плавно
     прокрутить страницу в самый верх.
    Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle()
    (прятать и показывать по клику) для данной секции.*/

$(function () {

    $(window).on("scroll", function () {
        if ($(window).scrollTop() < $(window).height()) {
            $("#scrollButton").hide();
        } else {
            $("#scrollButton").show();
        }

    });

    $("#menu").on("click","a", function (event) {
        event.preventDefault();

        let id  = $(this).attr('href');
        let top = $(id).offset().top;

        $('body,html').animate({scrollTop: top}, 300);

    });

//toggle message_body
    $("#button-toggle").on("click", function(){
        $("#MostPopularPosts").slideToggle(500);
        if ($(this).text() === "hide section") {
            $(this).text("show section");
        } else {
            $(this).text("hide section");
        }
            return false;
    });

//кнопка наверх

    $('.scrollUp').on("click", function(){
        $("html, body").animate({ scrollTop: 0 }, 300);
        return false;
    });



});






